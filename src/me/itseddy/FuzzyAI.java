package me.itseddy;

import mu.nu.nullpo.game.component.Controller;
import mu.nu.nullpo.game.component.Field;
import mu.nu.nullpo.game.component.Piece;
import mu.nu.nullpo.game.component.SpeedParam;
import mu.nu.nullpo.game.component.WallkickResult;
import mu.nu.nullpo.game.event.EventReceiver;
import mu.nu.nullpo.game.play.GameEngine;
import mu.nu.nullpo.game.play.GameManager;
import mu.nu.nullpo.game.subsystem.ai.DummyAI;
import mu.nu.nullpo.util.GeneralUtil;

import org.apache.log4j.Logger;

import com.fuzzylite.Engine;
import com.fuzzylite.FuzzyLite;
import com.fuzzylite.Op;
import com.fuzzylite.defuzzifier.Centroid;
import com.fuzzylite.imex.FldExporter;
import com.fuzzylite.norm.s.Maximum;
import com.fuzzylite.norm.t.Minimum;
import com.fuzzylite.rule.Rule;
import com.fuzzylite.rule.RuleBlock;
import com.fuzzylite.term.*;
import com.fuzzylite.variable.InputVariable;
import com.fuzzylite.variable.OutputVariable;

/**
 * PoochyBot AI
 */
public class FuzzyAI extends DummyAI implements Runnable {
	/** Log */
	static Logger log = Logger.getLogger(FuzzyAI.class);

	/** 接地したあとのX-coordinate */
	public int bestXSub;

	/** 接地したあとのY-coordinate */
	public int bestYSub;

	/** 接地したあとのDirection(-1: None) */
	public int bestRtSub;

	/** 最善手のEvaluation score */
	public int bestPts;

	/** 移動を遅らせる用の変count */
	public int delay;

	/** The GameEngine that owns this AI */
	public GameEngine gEngine;

	/** The GameManager that owns this AI */
	public GameManager gManager;

	/** When true,スレッドにThink routineの実行を指示 */
	public ThinkRequestMutex thinkRequest;

	/** true when thread is executing the think routine. */
	public boolean thinking;

	/** スレッドを停止させる time */
	public int thinkDelay;

	/** When true,スレッド動作中 */
	public volatile boolean threadRunning;

	/** Thread for executing the think routine */
	public Thread thread;

	/** Number of frames for which piece has been stuck */
	protected int stuckDelay;

	/** Status of last frame */
	protected int lastInput, lastX, lastY, lastRt;
	/** Number of consecutive frames with same piece status */
	protected int sameStatusTime;
	/** DAS charge status. -1 = left, 0 = none, 1 = right */
	protected int setDAS;
	/** Last input if done in ARE */
	protected int inputARE;
	/** Set to true to print debug information */
	protected static final boolean DEBUG_ALL = true;
	/** Wait extra frames at low speeds? */
	//protected static final boolean DELAY_DROP_ON = false;
	/** # of extra frames to wait */
	//protected static final int DROP_DELAY = 2;
	/** Number of frames waited */
	//protected int dropDelay;
	/** Did the thinking thread find a possible position? */
	protected boolean thinkSuccess;
	/** Was the game in ARE as of the last frame? */
	protected boolean inARE;

	protected GameEngine opponentEngine;

	protected int nextPieceBase;
	/*
	 * AI's name
	 */
	public String getName() {
		return "FuzztAI";
	}

	/*
	 * Called at initialization
	 */
	public void init(GameEngine engine, int playerID) {
		delay = 0;
		gEngine = engine;
		gManager = engine.owner;
		thinkRequest = new ThinkRequestMutex();
		thinking = false;
		threadRunning = false;
		setDAS = 0;

		stuckDelay = 0;
		inputARE = 0;
		lastInput = 0;
		lastX = -1;
		lastY = -1;
		lastRt = -1;
		sameStatusTime = 0;
		//dropDelay = 0;
		thinkComplete = false;
		thinkSuccess = false;
		inARE = false;

		if( ((thread == null) || !thread.isAlive()) && (engine.aiUseThread) ) {
			thread = new Thread(this, "AI_" + playerID);
			thread.setDaemon(true);
			thread.start();
			thinkDelay = engine.aiThinkDelay;
			thinkCurrentPieceNo = 0;
			thinkLastPieceNo = 0;
		}
		
		if(engine.owner.getPlayers() >= 2){
			if(playerID == 0){
				opponentEngine = engine.owner.engine[1];
			}
			else if(playerID == 1){
				opponentEngine = engine.owner.engine[0];
			}else{
				if (DEBUG_ALL) log.debug("No opponent Found!");
			}
			opponentEngine.subNewPieceAI = this;
		}else{
			if (DEBUG_ALL) log.debug("No opponent Found!");
		}
	}
	
	public void opponentPlaced(){
		//rethink
		thinkBestPosition(gEngine, gEngine.playerID);
	}

	/*
	 * 終了処理
	 */
	public void shutdown(GameEngine engine, int playerID) {
		if((thread != null) && (thread.isAlive())) {
			thread.interrupt();
			threadRunning = false;
			thread = null;
		}
	}

	/*
	 * Called whenever a new piece is spawned
	 */
	public void newPiece(GameEngine engine, int playerID) {
		if(!engine.aiUseThread) {
			thinkBestPosition(engine, playerID);
		} else if ((!thinking && !thinkComplete) || !engine.aiPrethink || engine.aiShowHint
				|| engine.getARE() <= 0 || engine.getARELine() <= 0) {
			thinkComplete = false;
			//thinkCurrentPieceNo++;
			thinkRequest.newRequest();
		}
	}

	/*
	 * Called at the start of each frame
	 */
	public void onFirst(GameEngine engine, int playerID) {
		inputARE = 0;
		boolean newInARE = engine.stat == GameEngine.Status.ARE;
		if ((engine.aiPrethink && engine.getARE() > 0 && engine.getARELine() > 0)
				&& ((newInARE && !inARE) || (!thinking && !thinkSuccess)))
		{
			if (DEBUG_ALL) log.debug("Begin pre-think of next piece.");
			thinkComplete = false;
			thinkRequest.newRequest();
		}
		inARE = newInARE;
		if(inARE && delay >= engine.aiMoveDelay) {
			int input = 0;
			Piece nextPiece = engine.getNextObject(engine.nextPieceCount);
			if (bestHold && thinkComplete)
			{
				input |= Controller.BUTTON_BIT_D;
				if (engine.holdPieceObject == null)
					nextPiece = engine.getNextObject(engine.nextPieceCount+1);
				else
					nextPiece = engine.holdPieceObject;
			}
			if (nextPiece == null)
				return;
			nextPiece = checkOffset(nextPiece, engine);
			input |= calcIRS(nextPiece, engine);
			if (threadRunning && !thinking && thinkComplete)
			{
				int spawnX = engine.getSpawnPosX(engine.field, nextPiece);
				if(bestX - spawnX > 1) {
					// left
					//setDAS = -1;
					input |= Controller.BUTTON_BIT_LEFT;
				} else if(spawnX - bestX > 1) {
					// right
					//setDAS = 1;
					input |= Controller.BUTTON_BIT_RIGHT;
				}
				else
					setDAS = 0;
				delay = 0;
			}
			if (DEBUG_ALL) log.debug("Currently in ARE. Next piece type = " +
					Piece.PIECE_NAMES[nextPiece.id] + ", IRS = " + input);
			//engine.ctrl.setButtonBit(input);
			inputARE = input;
		}
	}

	/*
	 * Called after every frame
	 */
	public void onLast(GameEngine engine, int playerID) {
	}

	/*
	 * Set button input states
	 */
	public void setControl(GameEngine engine, int playerID, Controller ctrl) {
		if( (engine.nowPieceObject != null) && (engine.stat == GameEngine.Status.MOVE) &&
			(delay >= engine.aiMoveDelay) && (engine.statc[0] > 0) &&
		    (!engine.aiUseThread || (threadRunning && !thinking && thinkComplete)))
		{
			inputARE = 0;
			int input = 0;	// Button input data
			Piece pieceNow = checkOffset(engine.nowPieceObject, engine);
			int nowX = engine.nowPieceX;
			int nowY = engine.nowPieceY;
			int rt = pieceNow.direction;
			Field fld = engine.field;
			boolean pieceTouchGround = pieceNow.checkCollision(nowX, nowY + 1, fld);
			int nowType = pieceNow.id;
			int width = fld.getWidth();

			int moveDir = 0; //-1 = left,  1 = right
			int rotateDir = 0; //-1 = left,  1 = right
			int drop = 0; //1 = up, -1 = down
			boolean sync = false; //true = delay either rotate or movement for synchro move if needed.

			//SpeedParam speed = engine.speed;
			//boolean lowSpeed = speed.gravity < speed.denominator;
			boolean canFloorKick = engine.nowUpwardWallkickCount < engine.ruleopt.rotateMaxUpwardWallkick
				|| engine.ruleopt.rotateMaxUpwardWallkick < 0;

			if ((rt == Piece.DIRECTION_DOWN &&
					((nowType == Piece.PIECE_L && bestX > nowX) || (nowType == Piece.PIECE_J && bestX < nowX))
					&& !fld.getBlockEmpty(pieceNow.getMaximumBlockX()+nowX-1, pieceNow.getMaximumBlockY()+nowY)))
			{
				thinkComplete = false;
				if (DEBUG_ALL) log.debug("Needs rethink - L or J piece is stuck!");
				thinkRequest.newRequest();
			}
			if (nowType == Piece.PIECE_O && ((bestX < nowX && pieceNow.checkCollision(nowX-1, nowY, rt, fld))
					|| (bestX < nowX && pieceNow.checkCollision(nowX-1, nowY, rt, fld))))
			{
				thinkComplete = false;
				if (DEBUG_ALL) log.debug("Needs rethink - O piece is stuck!");
				thinkRequest.newRequest();
			}
			if (pieceTouchGround && rt == bestRt &&
					(pieceNow.getMostMovableRight(nowX, nowY, rt, engine.field) < bestX ||
					pieceNow.getMostMovableLeft(nowX, nowY, rt, engine.field) > bestX))
				stuckDelay++;
			else
				stuckDelay = 0;
			if (stuckDelay > 4)
			{
				thinkComplete = false;
				if (DEBUG_ALL) log.debug("Needs rethink - piece is stuck!");
				thinkRequest.newRequest();
			}
			if (nowX == lastX && nowY == lastY && rt == lastRt && lastInput != 0)
			{
				sameStatusTime++;
				if (sameStatusTime > 4)
				{
					thinkComplete = false;
					if (DEBUG_ALL) log.debug("Needs rethink - piece is stuck, last inputs had no effect!");
					thinkRequest.newRequest();
				}
			}
			if (engine.nowPieceRotateCount >= 8)
			{
				thinkComplete = false;
				if (DEBUG_ALL) log.debug("Needs rethink - piece is stuck, too many rotations!");
				thinkRequest.newRequest();
			}
			else
				sameStatusTime = 0;
			if((bestHold == true) && thinkComplete && engine.isHoldOK()) {
				// Hold
				input |= Controller.BUTTON_BIT_D;

				Piece holdPiece = engine.holdPieceObject;
				if (holdPiece != null)
					input |= calcIRS(holdPiece, engine);
			} else {
				if (DEBUG_ALL) log.debug("bestX = " + bestX + ", nowX = " + nowX +
						", bestY = " + bestY + ", nowY = " + nowY +
						", bestRt = " + bestRt + ", rt = " + rt +
						", bestXSub = " + bestXSub + ", bestYSub = " + bestYSub + ", bestRtSub = " + bestRtSub);
				printPieceAndDirection(nowType, rt);
				// Rotation
				//Rotate iff near destination or stuck
				int xDiff = Math.abs(nowX - bestX);
				if (bestX < nowX && nowType == Piece.PIECE_I &&
						rt == Piece.DIRECTION_DOWN && bestRt != rt)
					xDiff--;
				boolean best180 = Math.abs(rt - bestRt) == 2;
				//Special movements for I piece
				if (nowType == Piece.PIECE_I)
				{
					int hypRtDir = 1;
					boolean rotateI = false;
					if ((rt+3)%4 == bestRt)
						hypRtDir = -1;
					if (nowX < bestX)
					{
						moveDir = 1;
						if (pieceNow.checkCollision(nowX+1, nowY, fld))
						{
							if((rt&1) == 0 && (canFloorKick || !pieceNow.checkCollision(nowX, nowY, (rt+1)%4, fld)))
								rotateI = true;
							else if ((rt&1) == 1 && canFloorKick)
								rotateI = true;
							else if (engine.isHoldOK() && !ctrl.isPress(Controller.BUTTON_D))
							{
								if (DEBUG_ALL) log.debug("Stuck I piece - use hold");
								input |= Controller.BUTTON_BIT_D;

								Piece holdPiece = engine.holdPieceObject;
								if (holdPiece != null)
									input |= calcIRS(holdPiece, engine);
							}
						}
					}
					else if (nowX > bestX)
					{
						moveDir = -1;
						if (pieceNow.checkCollision(nowX-1, nowY, fld))
						{
							if((rt&1) == 0 && (canFloorKick || !pieceNow.checkCollision(nowX, nowY, (rt+1)%4, fld)))
								rotateI = true;
							else if ((rt&1) == 1 && !pieceNow.checkCollision(nowX-1, nowY, (rt+1)%4, fld) &&
									canFloorKick)
								rotateI = true;
							else if (engine.isHoldOK() && !ctrl.isPress(Controller.BUTTON_D))
							{
								if (DEBUG_ALL) log.debug("Stuck I piece - use hold");
								input |= Controller.BUTTON_BIT_D;

								Piece holdPiece = engine.holdPieceObject;
								if (holdPiece != null)
									input |= calcIRS(holdPiece, engine);
							}
						}
					}
					else if (rt != bestRt)
					{
						if (best180)
							bestRt = (bestRt+2)%4;
						else
							rotateI = true;
					}
					if (rotateI)
						rotateDir = hypRtDir;
				}
				else if((rt != bestRt && ((xDiff <= 1) ||
						(bestX == 0 && nowX == 2 && nowType == Piece.PIECE_I) ||
						(((nowX < bestX && pieceNow.checkCollision(nowX+1, nowY, rt, fld)) ||
						(nowX > bestX && pieceNow.checkCollision(nowX-1, nowY, rt, fld))) &&
						!(pieceNow.getMaximumBlockX()+nowX == width-2 && (rt&1) == 1) &&
						!(pieceNow.getMinimumBlockY()+nowY == 2 && pieceTouchGround && (rt&1) == 0 && nowType != Piece.PIECE_I)))))
				{
					//if (DEBUG_ALL) log.debug("Case 1 rotation");

					int lrot = engine.getRotateDirection(-1);
					int rrot = engine.getRotateDirection(1);
					if (DEBUG_ALL) log.debug("lrot = " + lrot + ", rrot = " + rrot);

					if(best180 && (engine.ruleopt.rotateButtonAllowDouble) && !ctrl.isPress(Controller.BUTTON_E))
						input |= Controller.BUTTON_BIT_E;
					else if (bestRt == rrot)
						rotateDir = 1;
					else if(bestRt == lrot)
						rotateDir = -1;
					else if (engine.ruleopt.rotateButtonAllowReverse && best180 && (rt&1) == 1)
					{
						if(rrot == Piece.DIRECTION_UP)
							rotateDir = 1;
						else
							rotateDir = -1;
					}
					else
						rotateDir = 1;
				}
				//Try to keep flat side down on L, J, or T piece.
				else if (((rt != Piece.DIRECTION_UP && xDiff > 1 && engine.ruleopt.rotateButtonAllowReverse) /*|| best180*/) &&
						(nowType == Piece.PIECE_L || nowType == Piece.PIECE_J || nowType == Piece.PIECE_T))
				{
					//if (DEBUG_ALL) log.debug("Case 2 rotation");

					if (rt == Piece.DIRECTION_DOWN)
					{
						if (engine.ruleopt.rotateButtonAllowDouble && !ctrl.isPress(Controller.BUTTON_E))
							input |= Controller.BUTTON_BIT_E;
						else if (nowType == Piece.PIECE_L)
							rotateDir = -1;
						else if (nowType == Piece.PIECE_J)
							rotateDir = 1;
						else if (nowType == Piece.PIECE_T)
						{
							if (nowX > bestX)
								rotateDir = -1;
							else if (nowX < bestX)
								rotateDir = 1;
						}
					}
					else if (rt == Piece.DIRECTION_RIGHT)
						rotateDir = -1;
					else if (rt == Piece.DIRECTION_LEFT)
						rotateDir = 1;
				}

				// 到達可能な位置かどうか
				int minX = pieceNow.getMostMovableLeft(nowX, nowY, rt, fld);
				int maxX = pieceNow.getMostMovableRight(nowX, nowY, rt, fld);

				if( ((bestX < minX - 1) || (bestX > maxX + 1) || (bestY < nowY)) && (rt == bestRt) ) {
					// 到達不能なので再度思考する
					//thinkBestPosition(engine, playerID);
					thinkComplete = false;
					//thinkCurrentPieceNo++;
					//System.out.println("rethink c:" + thinkCurrentPieceNo + " l:" + thinkLastPieceNo);
					if (DEBUG_ALL) log.debug("Needs rethink - cannot reach desired position");
					thinkRequest.newRequest();
				} else {
					// 到達できる場合
					if((nowX == bestX) && (pieceTouchGround)) {
						if (rt == bestRt) {
							// 接地rotation
							if(bestRtSub != -1) {
								bestRt = bestRtSub;
								bestRtSub = -1;
							}
							// ずらし移動
							if(bestX != bestXSub) {
								bestX = bestXSub;
								bestY = bestYSub;
							}
						}
						else if (nowType == Piece.PIECE_I && (rt & 1) == 1 &&
								nowX+pieceNow.getMaximumBlockX() == width-2 && (fld.getHighestBlockY() <= 4 ||
									(fld.getHighestBlockY(width-2) - fld.getHighestBlockY(width-1) >=4 )))
						{
							bestRt = rt;
							bestX++;
						}
					}
					/*
					//Move left if need to move left, or if at rightmost position and can move left.
					if (pieceTouchGround && pieceNow.id != Piece.PIECE_I &&
							nowX+pieceNow.getMaximumBlockX() == width-1 &&
							!pieceNow.checkCollision(nowX-1, nowY, fld))
					{
						if(!ctrl.isPress(Controller.BUTTON_LEFT) && (engine.aiMoveDelay >= 0))
							input |= Controller.BUTTON_BIT_LEFT;
						bestX = nowX - 1;
					}
					*/
					if (nowX > bestX)
						moveDir = -1;
					else if(nowX < bestX)
						moveDir = 1;
					else if((nowX == bestX) && (rt == bestRt)) {
						moveDir = 0;
						setDAS = 0;
						// 目標到達
						if((bestRtSub == -1) && (bestX == bestXSub)) {
							if (pieceTouchGround && engine.ruleopt.softdropLock)
								drop = -1;
							else if(engine.ruleopt.harddropEnable)
								drop = 1;
							else if(engine.ruleopt.softdropEnable || engine.ruleopt.softdropLock)
								drop = -1;
						} else {
							if(engine.ruleopt.harddropEnable && !engine.ruleopt.harddropLock)
								drop = 1;
							else if(engine.ruleopt.softdropEnable && !engine.ruleopt.softdropLock)
								drop = -1;
						}
					}
				}
			}

			int minBlockX = nowX+pieceNow.getMinimumBlockX();
			int maxBlockX = nowX+pieceNow.getMaximumBlockX();
			int minBlockXDepth = fld.getHighestBlockY(minBlockX);
			int maxBlockXDepth = fld.getHighestBlockY(maxBlockX);
			if (nowType == Piece.PIECE_L && minBlockXDepth < maxBlockXDepth && pieceTouchGround
					&& rt == Piece.DIRECTION_DOWN && rotateDir == -1 && maxBlockX < width-1)
			{
				if (bestX == nowX+1)
					moveDir = 1;
				else if (bestX < nowX)
				{
					if (DEBUG_ALL) log.debug("Delaying rotation on L piece to avoid getting stuck. (Case 1)");
					sync = false;
					rotateDir = 0;
					moveDir = 1;
				}
				else if (bestX > nowX)
				{
					/*
					if (minBlockXDepth == fld.getHighestBlockY(minBlockX-1))
					{
						if (DEBUG_ALL) log.debug("Delaying rotation on L piece to avoid getting stuck. (Case 2)");
						sync = false;
						rotateDir = 0;
						moveDir = -1;
					}
					else
					*/
					if (DEBUG_ALL) log.debug("Attempting synchro move on L piece to avoid getting stuck.");
					sync = true;
					rotateDir = -1;
					moveDir = -1;
				}
			}
			else if (nowType == Piece.PIECE_J && minBlockXDepth > maxBlockXDepth && pieceTouchGround
					&& rt == Piece.DIRECTION_DOWN && rotateDir == 1 && minBlockX > 0)
			{
				if (bestX == nowX-1)
					moveDir = -1;
				else if (bestX > nowX)
				{
					if (DEBUG_ALL) log.debug("Delaying rotation on J piece to avoid getting stuck. (Case 1)");
					sync = false;
					rotateDir = 0;
					moveDir = -1;
				}
				else if (bestX < nowX)
				{
					/*
					if (maxBlockXDepth == fld.getHighestBlockY(maxBlockX+1))
					{
						if (DEBUG_ALL) log.debug("Delaying rotation on J piece to avoid getting stuck. (Case 2)");
						sync = false;
						rotateDir = 0;
						moveDir = 1;
					}
					else
					*/
					if (DEBUG_ALL) log.debug("Attempting synchro move on J piece to avoid getting stuck.");
					sync = true;
					rotateDir = 1;
					moveDir = 1;
				}
			}
			else if (rotateDir != 0 && moveDir != 0 && pieceTouchGround && (rt&1) == 1
					&& (nowType == Piece.PIECE_J || nowType == Piece.PIECE_L)
					&& !pieceNow.checkCollision(nowX+moveDir, nowY+1, rt, fld))
			{
				if (DEBUG_ALL) log.debug("Delaying move on L or J piece to avoid getting stuck.");
				sync = false;
				moveDir = 0;
			}
			if (engine.nowPieceRotateCount >= 5 && rotateDir != 0 && moveDir != 0 && !sync)
			{
				if (DEBUG_ALL) log.debug("Piece seems to be stuck due to unintentional synchro - trying intentional desync.");
				moveDir = 0;
			}
			if (moveDir == -1 && minBlockX == 1 && nowType == Piece.PIECE_I && (rt&1) == 1
					&& pieceNow.checkCollision(nowX-1, nowY, rt, fld))
			{
				int depthNow = fld.getHighestBlockY(minBlockX);
				int depthLeft = fld.getHighestBlockY(minBlockX-1);
				if(depthNow > depthLeft && depthNow - depthLeft < 2)
				{
					if (!pieceNow.checkCollision(nowX+1, nowY, rt, fld))
						moveDir = 1;
					else if (engine.isHoldOK() && !ctrl.isPress(Controller.BUTTON_D))
						input |= Controller.BUTTON_BIT_D;
				}
			}
			/*
			//Catch bug where it fails to rotate J piece
			if (moveDir == 0 && rotateDir == 0 & drop == 0)
			{
				if ((rt+1)%4 == bestRt)
					rotateDir = 1;
				else if ((rt+3)%4 == bestRt)
					rotateDir = -1;
				else if ((rt+2)%4 == bestRt)
				{
					if(engine.ruleopt.rotateButtonAllowDouble)
						rotateDir = 2;
					else if (rt == 3)
						rotateDir = -1;
					else
						rotateDir = -1;
				}
				else if (bestX < nowX)
					moveDir = -1;
				else if (bestX > nowX)
					moveDir = 1;
				else
					if (DEBUG_ALL) log.debug("Movement error: Nothing to do!");
			}
			if (rotateDir == 0 && Math.abs(rt - bestRt) == 2)
				rotateDir = 1;
			*/
			//Convert parameters to input
			boolean useDAS = engine.dasCount >= engine.getDAS() && moveDir == setDAS;
			if(moveDir == -1 && (!ctrl.isPress(Controller.BUTTON_LEFT) || useDAS))
				input |= Controller.BUTTON_BIT_LEFT;
			else if(moveDir == 1 && (!ctrl.isPress(Controller.BUTTON_RIGHT) || useDAS))
				input |= Controller.BUTTON_BIT_RIGHT;
			/*
			if(drop == 1 && !ctrl.isPress(Controller.BUTTON_UP))
			{
				if (DELAY_DROP_ON && lowSpeed && dropDelay < (DROP_DELAY >> 1))
					dropDelay++;
				else
					input |= Controller.BUTTON_BIT_UP;
			}
			else if(drop == -1)
			{
				if (DELAY_DROP_ON && lowSpeed && dropDelay < DROP_DELAY)
					dropDelay++;
				else
					input |= Controller.BUTTON_BIT_DOWN;
			}
			*/
			if(drop == 1 && !ctrl.isPress(Controller.BUTTON_UP))
				input |= Controller.BUTTON_BIT_UP;
			else if(drop == -1)
				input |= Controller.BUTTON_BIT_DOWN;

			if (rotateDir != 0)
			{
				boolean defaultRotateRight = (engine.owRotateButtonDefaultRight == 1 ||
						(engine.owRotateButtonDefaultRight == -1 &&
								engine.ruleopt.rotateButtonDefaultRight));
				
				if(engine.ruleopt.rotateButtonAllowDouble &&
						rotateDir == 2 && !ctrl.isPress(Controller.BUTTON_E))
					input |= Controller.BUTTON_BIT_E;
				else if(engine.ruleopt.rotateButtonAllowReverse &&
						  !defaultRotateRight && (rotateDir == 1))
				{
					if(!ctrl.isPress(Controller.BUTTON_B))
						input |= Controller.BUTTON_BIT_B;
				}
				else if(engine.ruleopt.rotateButtonAllowReverse &&
						defaultRotateRight && (rotateDir == -1))
				{
					if(!ctrl.isPress(Controller.BUTTON_B))
						input |= Controller.BUTTON_BIT_B;
				}
				else if(!ctrl.isPress(Controller.BUTTON_A))
					input |= Controller.BUTTON_BIT_A;
			}
			if (sync)
			{
				if (DEBUG_ALL) log.debug("Attempting to perform synchro move.");
				int bitsLR = Controller.BUTTON_BIT_LEFT | Controller.BUTTON_BIT_RIGHT;
				int bitsAB = Controller.BUTTON_BIT_A | Controller.BUTTON_BIT_B;
				if ((input & bitsLR) == 0 || (input & bitsAB) == 0)
				{
					setDAS = 0;
					input &= ~(bitsLR | bitsAB);
				}
			}
			if (setDAS != moveDir)
				setDAS = 0;

			lastInput = input;
			lastX = nowX;
			lastY = nowY;
			lastRt = rt;

			if (DEBUG_ALL) log.debug ("Input = " + input + ", moveDir = " + moveDir  + ", rotateDir = " + rotateDir +
					 ", sync = " + sync  + ", drop = " + drop  + ", setDAS = " + setDAS);

			delay = 0;
			ctrl.setButtonBit(input);
		}
		else {
			//dropDelay = 0;
			delay++;
			ctrl.setButtonBit(inputARE);
		}
	}

	protected void printPieceAndDirection(int pieceType, int rt)
	{
		String result = "Piece " + Piece.PIECE_NAMES[pieceType] + ", direction ";

		switch (rt)
		{
			case Piece.DIRECTION_LEFT:  result = result + "left";  break;
			case Piece.DIRECTION_DOWN:  result = result + "down";  break;
			case Piece.DIRECTION_UP:    result = result + "up";    break;
			case Piece.DIRECTION_RIGHT: result = result + "right"; break;
		}
		if (DEBUG_ALL) log.debug(result);
	}

	public int calcIRS(Piece piece, GameEngine engine)
	{
		piece = checkOffset(piece, engine);
		int nextType = piece.id;
		Field fld = engine.field;
		int spawnX = engine.getSpawnPosX(fld, piece);
		SpeedParam speed = engine.speed;
		boolean gravityHigh = speed.gravity > speed.denominator;
		int width = fld.getWidth();
		int midColumnX = (width/2)-1;
		if(Math.abs(spawnX - bestX) == 1)
		{
			if (bestRt == 1)
			{
				if (engine.ruleopt.rotateButtonDefaultRight)
					return Controller.BUTTON_BIT_A;
				else
					return Controller.BUTTON_BIT_B;
			}
			else if (bestRt == 3)
			{
				if (engine.ruleopt.rotateButtonDefaultRight)
					return Controller.BUTTON_BIT_B;
				else
					return Controller.BUTTON_BIT_A;
			}
		}
		else if (nextType == Piece.PIECE_L)
		{
			if (gravityHigh && fld.getHighestBlockY(midColumnX-1) <
					Math.min(fld.getHighestBlockY(midColumnX), fld.getHighestBlockY(midColumnX+1)))
				return 0;
			else if (engine.ruleopt.rotateButtonDefaultRight)
				return Controller.BUTTON_BIT_B;
			else
				return Controller.BUTTON_BIT_A;
		}
		else if (nextType == Piece.PIECE_J)
		{
			if (gravityHigh && fld.getHighestBlockY(midColumnX+1) <
					Math.min(fld.getHighestBlockY(midColumnX), fld.getHighestBlockY(midColumnX-1)))
				return 0;
			if (engine.ruleopt.rotateButtonDefaultRight)
				return Controller.BUTTON_BIT_A;
			else
				return Controller.BUTTON_BIT_B;
		}
		/*
		else if (nextType == Piece.PIECE_I)
			return Controller.BUTTON_BIT_A;
		*/
		return 0;
	}

	/**
	 * Search for the best choice
	 * @param engine The GameEngine that owns this AI
	 * @param playerID Player ID
	 */
	
	public  double max(double... n) {
	    int i = 0;
	    double max = n[i];

	    while (++i < n.length)
	        if (n[i] > max)
	            max = n[i];

	    return max;
	}
	
	
	public void thinkBestPosition(GameEngine engine, int playerID) {
		if (DEBUG_ALL) log.debug("thinkBestPosition called, inARE = " + inARE + ", piece: ");
		bestHold = false;
		bestX = 0;
		bestY = 0;
		bestRt = 0;
		bestPts = 0;
		thinkSuccess = false;

		Field fld;
		if (engine.stat == GameEngine.Status.READY)
			fld = new Field(engine.fieldWidth, engine.fieldHeight,
					engine.fieldHiddenHeight, engine.ruleopt.fieldCeiling);
		else
			fld = new Field(engine.field);
		Piece pieceNow = engine.nowPieceObject;
		Piece pieceHold = engine.holdPieceObject;
		
		/*
		  get now state to display
		*/
		if (inARE || pieceNow == null)
		{
			pieceNow = engine.getNextObjectCopy(engine.nextPieceCount);
			nextPieceBase = engine.nextPieceCount + 1;
			if(pieceHold == null){
				pieceHold = engine.getNextObjectCopy(engine.nextPieceCount+1);
				nextPieceBase = engine.nextPieceCount + 2;
			}
		}
		else {
			if (pieceHold == null){
				pieceHold = engine.getNextObjectCopy(engine.nextPieceCount);
				nextPieceBase = engine.nextPieceCount + 1;
			}
		}
		
		pieceNow = checkOffset(pieceNow, engine);
		pieceHold = checkOffset(pieceHold, engine);
		
		MoveCollection move = recursiveSearch(!gEngine.isHoldOK(), fld, pieceNow, pieceHold, 0);
		
		
		/* fuzzy inference to choice move */
		fld.culculateField();
		
		Engine fuzzy_engine = new Engine();	
		
		InputVariable fuzzy_bumpiness = new InputVariable();
		fuzzy_bumpiness.setName("bumpiness");
		fuzzy_bumpiness.setRange(0.000, 15.000);
		fuzzy_bumpiness.addTerm(new Trapezoid("LOW", 0.0, 0.1, 1.5, 2.5));
		fuzzy_bumpiness.addTerm(new Trapezoid("MEDIUM", 2.0, 3.0, 4.0, 5.0));
		fuzzy_bumpiness.addTerm(new Trapezoid("HIGH", 4.0, 6.0, 14.9, 15.0));
		fuzzy_engine.addInputVariable(fuzzy_bumpiness);
        fuzzy_bumpiness.setInputValue(fld.bumpiness);
        
		InputVariable fuzzy_height = new InputVariable();
		fuzzy_height.setName("height");
		fuzzy_height.setRange(0.000, 23.000);
		fuzzy_height.addTerm(new Trapezoid("LOW", 0.0, 2.5, 7.5, 10.0));
		fuzzy_height.addTerm(new Trapezoid("MEDIUM", 7.0, 9.5, 14.5, 17.0));
		fuzzy_height.addTerm(new Trapezoid("HIGH", 13.0, 15.5, 20.5, 23.0));
		fuzzy_engine.addInputVariable(fuzzy_height);
		fuzzy_height.setInputValue(fld.average_height);
        
        
        InputVariable fuzzy_hole_rate[] = new InputVariable[3];
        
        fuzzy_hole_rate[0] = new InputVariable();
        fuzzy_hole_rate[0].setName("hole_rate_low");
        fuzzy_hole_rate[0].setRange(0.000, 50.000);
        fuzzy_hole_rate[0].addTerm(new Trapezoid("LOW", 0.0, 0.1, 2.0, 5.0));
        fuzzy_hole_rate[0].addTerm(new Trapezoid("MEDIUM", 3.5, 5.0, 7.0, 12.0));
        fuzzy_hole_rate[0].addTerm(new Trapezoid("HIGH", 6.0, 9.0, 40.0, 50.0));
        fuzzy_engine.addInputVariable(fuzzy_hole_rate[0]);
        fuzzy_hole_rate[0].setInputValue(fld.hole_rate[0]);
        
        fuzzy_hole_rate[1] = new InputVariable();
        fuzzy_hole_rate[1].setName("hole_rate_medium");
        fuzzy_hole_rate[1].setRange(0.000, 50.000);
        fuzzy_hole_rate[1].addTerm(new Trapezoid("LOW", 0.0, 0.1, 2.0, 5.0));
        fuzzy_hole_rate[1].addTerm(new Trapezoid("MEDIUM", 3.5, 5.0, 7.0, 12.0));
        fuzzy_hole_rate[1].addTerm(new Trapezoid("HIGH", 6.0, 9.0, 40.0, 50.0));
        fuzzy_engine.addInputVariable(fuzzy_hole_rate[1]);
        fuzzy_hole_rate[1].setInputValue(fld.hole_rate[1]);
        
        fuzzy_hole_rate[2] = new InputVariable();
        fuzzy_hole_rate[2].setName("hole_rate_high");
        fuzzy_hole_rate[2].setRange(0.000, 50.000);
        fuzzy_hole_rate[2].addTerm(new Trapezoid("LOW", 0.0, 0.1, 2.0, 5.0));
        fuzzy_hole_rate[2].addTerm(new Trapezoid("MEDIUM", 3.5, 5.0, 7.0, 12.0));
        fuzzy_hole_rate[2].addTerm(new Trapezoid("HIGH", 6.0, 9.0, 40.0, 50.0));
        fuzzy_engine.addInputVariable(fuzzy_hole_rate[2]);
        fuzzy_hole_rate[2].setInputValue(fld.hole_rate[2]);		
		
		OutputVariable fuzzy_sendline = new OutputVariable();
		fuzzy_sendline.setName("Sendline");
		fuzzy_sendline.setRange(0.000, 1.000);
		fuzzy_sendline.setDefaultValue(Double.NaN);
		fuzzy_sendline.addTerm(new Triangle("LOW", 0.000, 0.250, 0.500));
		fuzzy_sendline.addTerm(new Triangle("MEDIUM", 0.250, 0.500, 0.750));
		fuzzy_sendline.addTerm(new Triangle("HIGH", 0.500, 0.750, 1.000));     
        
        OutputVariable fuzzy_stack = new OutputVariable();
        fuzzy_stack.setName("Stack");
        fuzzy_stack.setRange(0.000, 1.000);
        fuzzy_stack.setDefaultValue(Double.NaN);
        fuzzy_stack.addTerm(new Triangle("LOW", 0.000, 0.250, 0.500));
        fuzzy_stack.addTerm(new Triangle("MEDIUM", 0.250, 0.500, 0.750));
        fuzzy_stack.addTerm(new Triangle("HIGH", 0.500, 0.750, 1.000));   
        
        OutputVariable fuzzy_downstack = new OutputVariable();
        fuzzy_downstack.setName("Downstack");
        fuzzy_downstack.setRange(0.000, 1.000);
        fuzzy_downstack.setDefaultValue(Double.NaN);
        fuzzy_downstack.addTerm(new Triangle("LOW", 0.000, 0.250, 0.500));
        fuzzy_downstack.addTerm(new Triangle("MEDIUM", 0.250, 0.500, 0.750));
        fuzzy_downstack.addTerm(new Triangle("HIGH", 0.500, 0.750, 1.000));     
        
        OutputVariable fuzzy_wait = new OutputVariable();
        fuzzy_wait.setName("Wait");
        fuzzy_wait.setRange(0.000, 1.000);
        fuzzy_wait.setDefaultValue(Double.NaN);
        fuzzy_wait.addTerm(new Triangle("LOW", 0.000, 0.250, 0.500));
        fuzzy_wait.addTerm(new Triangle("MEDIUM", 0.250, 0.500, 0.750));
        fuzzy_wait.addTerm(new Triangle("HIGH", 0.500, 0.750, 1.000));
        
        fuzzy_engine.addOutputVariable(fuzzy_sendline);
        fuzzy_engine.addOutputVariable(fuzzy_stack);
        fuzzy_engine.addOutputVariable(fuzzy_downstack);
        fuzzy_engine.addOutputVariable(fuzzy_wait);
        
        
        //Fuzzy rules
        RuleBlock ruleBlock = new RuleBlock();
        ruleBlock.addRule(Rule.parse("if hole_rate_low is LOW then Stack is HIGH", fuzzy_engine));
        ruleBlock.addRule(Rule.parse("if hole_rate_low is MEDIUM then Stack is HIGH", fuzzy_engine));
        ruleBlock.addRule(Rule.parse("if hole_rate_low is HIGH then Stack is HIGH", fuzzy_engine));
        ruleBlock.addRule(Rule.parse("if hole_rate_low is LOW then Stack is HIGH", fuzzy_engine));
        ruleBlock.addRule(Rule.parse("if hole_rate_low is MEDIUM then Stack is HIGH", fuzzy_engine));
        ruleBlock.addRule(Rule.parse("if hole_rate_low is HIGH then Stack is HIGH", fuzzy_engine));
        ruleBlock.addRule(Rule.parse("if hole_rate_low is LOW then Stack is HIGH", fuzzy_engine));
        ruleBlock.addRule(Rule.parse("if hole_rate_low is MEDIUM then Stack is HIGH", fuzzy_engine));
        ruleBlock.addRule(Rule.parse("if hole_rate_low is HIGH then Stack is HIGH", fuzzy_engine));
        ruleBlock.addRule(Rule.parse("if hole_rate_low is LOW then Stack is HIGH", fuzzy_engine));
        ruleBlock.addRule(Rule.parse("if hole_rate_low is MEDIUM then Stack is HIGH", fuzzy_engine));
        ruleBlock.addRule(Rule.parse("if hole_rate_low is HIGH then Stack is HIGH", fuzzy_engine));
        ruleBlock.addRule(Rule.parse("if hole_rate_low is LOW then Stack is HIGH", fuzzy_engine));
        ruleBlock.addRule(Rule.parse("if hole_rate_low is MEDIUM then Stack is HIGH", fuzzy_engine));
        ruleBlock.addRule(Rule.parse("if hole_rate_low is HIGH then Stack is HIGH", fuzzy_engine));
        ruleBlock.addRule(Rule.parse("if hole_rate_low is LOW then Stack is HIGH", fuzzy_engine));
        ruleBlock.addRule(Rule.parse("if hole_rate_low is MEDIUM then Stack is HIGH", fuzzy_engine));
        ruleBlock.addRule(Rule.parse("if hole_rate_low is HIGH then Stack is HIGH", fuzzy_engine));
        fuzzy_engine.addRuleBlock(ruleBlock);
        
        fuzzy_engine.configure("", "", "Minimum", "Maximum", "Centroid");
               
        fuzzy_engine.process();
        
        double sendline = fuzzy_sendline.getOutputValue();
        double stack = fuzzy_stack.getOutputValue();
        double wait = fuzzy_wait.getOutputValue();
        double downstack = fuzzy_downstack.getOutputValue();
        double best = max(sendline, stack, wait, downstack);
        
        if (DEBUG_ALL){
        	log.debug("fuzzy_stack = " + fuzzy_stack.getOutputValue() );
        	log.debug("fuzzy_sendline = " + fuzzy_sendline.getOutputValue() );
        	log.debug("fuzzy_wait = " + fuzzy_wait.getOutputValue() );
        	log.debug("fuzzy_downstack = " + fuzzy_downstack.getOutputValue() );
        }
        
		if(best == sendline){
			
		}
		else if(best == stack){
			bestX = move.highestPts.x;
			bestY = move.highestPts.y;
			bestRt = move.highestPts.rt;
			bestPts = move.highestPts.pts;
			bestHold = move.highestPts.isHold;
		}
		else if(best == wait){
			
		}
		else if(best == downstack){
			
		}
		
	}
	
	final int MAXDEPTH = 3;
	final int MINDEPTH = 1;
	
	class PlaceCandicate{
		int x; int y; int rt; int pts; boolean isHold;
		PlaceCandicate(int x, int y, int rt, int pts){
			this.x = x;
			this.y = y;
			this.rt = rt;
			this.pts = pts;
			this.isHold = false;
		}
	}
	
	class MoveCollection{
		PlaceCandicate downstack;
		PlaceCandicate linesend;
		PlaceCandicate prepareTetris;
		PlaceCandicate prepareTSD;
		PlaceCandicate prepareTetrisM;
		PlaceCandicate prepareTSDM;
		PlaceCandicate prepareCombo;
		PlaceCandicate highestPts; /* fallback */
		MoveCollection(){
			downstack = new PlaceCandicate(0,0,0,0);
			linesend = new PlaceCandicate(Integer.MIN_VALUE,0,0,0);
			prepareTetris = new PlaceCandicate(Integer.MIN_VALUE,0,0,0);
			prepareTSD = new PlaceCandicate(Integer.MIN_VALUE,0,0,0);
			prepareTetrisM = new PlaceCandicate(Integer.MIN_VALUE,0,0,0);
			prepareTSDM = new PlaceCandicate(Integer.MIN_VALUE,0,0,0);
			prepareCombo = new PlaceCandicate(Integer.MIN_VALUE,0,0,0);
			highestPts = new PlaceCandicate(Integer.MIN_VALUE,0,0,Integer.MIN_VALUE);
		}
		boolean noSpecialMove(){
			return downstack.x == Integer.MIN_VALUE && linesend.x == Integer.MIN_VALUE &&
					prepareTetris.x == Integer.MIN_VALUE && prepareTSD.x == Integer.MIN_VALUE &&
					prepareTetrisM.x == Integer.MIN_VALUE && prepareTSDM.x == Integer.MIN_VALUE &&
					prepareCombo.x == Integer.MIN_VALUE;
		}
		void getHigherPts(MoveCollection collection){
			downstack = collection.downstack.pts > downstack.pts ? collection.downstack : downstack;
			linesend = collection.linesend.pts > linesend.pts ? collection.linesend : linesend;
			prepareTetris = collection.prepareTetris.pts > prepareTetris.pts ? collection.prepareTetris : prepareTetris;
			prepareTSD = collection.prepareTSD.pts > prepareTSD.pts ? collection.prepareTSD : prepareTSD;
			prepareTetrisM = collection.prepareTetrisM.pts > prepareTetrisM.pts ? collection.prepareTetrisM : prepareTetrisM;
			prepareTSDM = collection.prepareTSDM.pts > prepareTSDM.pts ? collection.prepareTSDM : prepareTSDM;
			prepareCombo = collection.prepareCombo.pts > prepareCombo.pts ? collection.prepareCombo : prepareCombo;
			highestPts = collection.highestPts.pts > highestPts.pts ? collection.highestPts : highestPts;
		}
		void AddFuturepts(MoveCollection collection){
			downstack.pts += collection.downstack.pts;
			/* linesend.pts unchanged... just need to know this move */
			prepareTetris.pts += collection.prepareTetris.pts;
			prepareTSD.pts += collection.prepareTSD.pts;
			if(prepareTetris.pts != 0 && collection.prepareTetris.pts != 0){
				prepareTetrisM.pts += collection.prepareTetrisM.pts + 1;
			}
			if(prepareTSD.pts != 0 && collection.prepareTSD.pts != 0){
				prepareTSDM.pts += collection.prepareTSDM.pts + 1;
			}
			if(linesend.pts != 0){
				prepareCombo.pts = collection.prepareCombo.pts + 1;
			}else{
				prepareCombo.pts = 0;
			}
			highestPts.pts += collection.highestPts.pts;
		}
		void isHold(){
			downstack.isHold = true;
			linesend.isHold = true;
			prepareTetris.isHold = true;
			prepareTSD.isHold = true;
			prepareTetrisM.isHold = true;
			prepareTSDM.isHold = true;
			prepareCombo.isHold = true;
			highestPts.isHold = true;
		}
	}
	
	protected MoveCollection EvaluatePiece(Piece piece, Piece curHold,int minX, int maxX, int nowX, int nowY, int rt, Field fld, int depth){
		MoveCollection ret = new MoveCollection();
		
		int nextDepth = depth + 1;
		
		for(int x = minX; x <= maxX; x++) {
			fld.copy(gEngine.field);
			int y = piece.getBottom(x, nowY, rt, fld);

			if(!piece.checkCollision(x, y, rt, fld)) {
				MoveCollection pts = thinkMain(x, y, rt, -1, fld, piece, depth);
				if(nextDepth < MINDEPTH || nextDepth < MAXDEPTH && pts.highestPts.pts < -10000){
					Piece next = gEngine.getNextObjectCopy(nextPieceBase + nextDepth);
					pts.AddFuturepts(recursiveSearch(false, fld, next, curHold, nextDepth));
				}
				ret.getHigherPts(pts);

				// Left rotation
				if(!gEngine.ruleopt.rotateButtonDefaultRight || gEngine.ruleopt.rotateButtonAllowReverse) {
					int rot = piece.getRotateDirection(-1, rt);
					int newX = x;
					int newY = y;
					boolean canrotate = false;
					fld.copy(gEngine.field);
					if(!piece.checkCollision(x, y, rot, fld)) {
						canrotate = true;
					} else if((gEngine.wallkick != null) && (gEngine.ruleopt.rotateWallkick)) {
						boolean allowUpward = (gEngine.ruleopt.rotateMaxUpwardWallkick < 0) ||
											  (gEngine.nowUpwardWallkickCount < gEngine.ruleopt.rotateMaxUpwardWallkick);
						WallkickResult kick = gEngine.wallkick.executeWallkick(x, y, -1, rt, rot,
											  allowUpward, piece, fld, null);

						if(kick != null) {
							newX = x + kick.offsetX;
							newY = y + kick.offsetY;
						}
						canrotate = true;
					}
					if(canrotate){
						pts = thinkMain(newX, newY, rot, rt, fld, piece, depth);
						if(nextDepth < MINDEPTH || nextDepth < MAXDEPTH && pts.highestPts.pts < -10000){
							Piece next = gEngine.getNextObjectCopy(nextPieceBase + nextDepth);
							pts.AddFuturepts(recursiveSearch(false, fld, next, curHold, nextDepth));
						}
						ret.getHigherPts(pts);
					}
				}

				// Right rotation
				if(gEngine.ruleopt.rotateButtonDefaultRight || gEngine.ruleopt.rotateButtonAllowReverse) {
					int rot = piece.getRotateDirection(1, rt);
					int newX = x;
					int newY = y;
					boolean canrotate = false;
					fld.copy(gEngine.field);
					if(!piece.checkCollision(x, y, rot, fld)) {
						canrotate = true;
					} else if((gEngine.wallkick != null) && (gEngine.ruleopt.rotateWallkick)) {
						boolean allowUpward = (gEngine.ruleopt.rotateMaxUpwardWallkick < 0) ||
											  (gEngine.nowUpwardWallkickCount < gEngine.ruleopt.rotateMaxUpwardWallkick);
						WallkickResult kick = gEngine.wallkick.executeWallkick(x, y, 1, rt, rot,
											  allowUpward, piece, fld, null);

						if(kick != null) {
							newX = x + kick.offsetX;
							newY = y + kick.offsetY;
						}
						canrotate = true;
					}
					if(canrotate){
						pts = thinkMain(newX, newY, rot, rt, fld, piece, depth);
						if(nextDepth < MINDEPTH || nextDepth < MAXDEPTH && pts.highestPts.pts < -10000){
							Piece next = gEngine.getNextObjectCopy(nextPieceBase + nextDepth);
							pts.AddFuturepts(recursiveSearch(false, fld, next, curHold, nextDepth));
						}
						ret.getHigherPts(pts);
					}
				}
			}
		}
		return ret;
	}
	
	protected MoveCollection recursiveSearch(boolean holded, Field fld, Piece pieceNow, Piece pieceHold, int depth){
		MoveCollection moves = new MoveCollection();
		
		/* determine capabilities */
		boolean holdOK = false;
		
		holdOK = !holded;
		
		for(int rt = 0; rt < Piece.DIRECTION_COUNT; rt++) {
			// Now piece
			int nowX = gEngine.getSpawnPosX(fld, pieceNow);
			int nowY = gEngine.getSpawnPosY(pieceNow);
			int minX = pieceNow.getMostMovableLeft(nowX, nowY, rt, gEngine.field);
			int maxX = pieceNow.getMostMovableRight(nowX, nowY, rt, gEngine.field);
			boolean spawnOK = true;
			if (gEngine.stat == GameEngine.Status.ARE)
			{
				int spawnX = gEngine.getSpawnPosX(fld, pieceNow);
				int spawnY = gEngine.getSpawnPosY(pieceNow);
				spawnOK = !pieceNow.checkCollision(spawnX, spawnY, fld);
			}
			if(spawnOK){
				MoveCollection nowMove = EvaluatePiece(pieceNow, pieceHold, minX, maxX, nowX, nowY, rt, fld, depth);
				moves.getHigherPts(nowMove);
			}

			// Hold piece
			if(holdOK) {
				int spawnX = gEngine.getSpawnPosX(gEngine.field, pieceHold);
				int spawnY = gEngine.getSpawnPosY(pieceHold);
				int minHoldX = pieceHold.getMostMovableLeft(spawnX, spawnY, rt, gEngine.field);
				int maxHoldX = pieceHold.getMostMovableRight(spawnX, spawnY, rt, gEngine.field);
				MoveCollection holdMove = EvaluatePiece(pieceHold, pieceNow, minHoldX, maxHoldX, spawnX, spawnY, rt, fld, depth);
				if(depth == 0) holdMove.isHold();
				moves.getHigherPts(holdMove);
			}
		}
		return moves;
	}
	

	/**
	 * Think routine
	 * @param x X-coordinate
	 * @param y Y-coordinate
	 * @param rt Direction
	 * @param rtOld Direction before rotation (-1: None）
	 * @param fld Field (Can be modified without problems)
	 * @param piece Piece
	 * @param depth Compromise level (ranges from 0 through getMaxThinkDepth-1)
	 * @return Evaluation score
	 */
	public MoveCollection thinkMain(int x, int y, int rt, int rtOld, Field fld, Piece piece, int depth) {
		MoveCollection pts = new MoveCollection();
		int mixPts = 0;

		int width = fld.getWidth();
		int holeBefore = fld.getHowManyHoles();
		int hchangeBefore = fld.heightchange();
		int varianceBefore = fld.variance();

		// T-Spin flag
		boolean tspin = false;
		if((piece.id == Piece.PIECE_T) && (rtOld != -1) && (fld.isTSpinSpot(x, y, piece.big))) {
			tspin = true;
		}

		// ピースを置く
		if(!piece.placeToField(x, y, rt, fld)) {
			pts.highestPts.x = x;
			pts.highestPts.y = y;
			pts.highestPts.rt = rt;
			pts.highestPts.pts = Integer.MIN_VALUE;
			return pts;
		}else{
			// Line clear
			int lines = fld.checkLine();
			if(lines > 0) {
				fld.clearLine();
				fld.downFloatingBlocks();
			}

			// Field height (after clears)
			int varianceAfter = fld.variance();
			int holeAfter = fld.getHowManyHoles();
			int hchangeAfter = fld.heightchange();
			
			if(varianceAfter < varianceBefore){
				pts.downstack.pts += varianceBefore - varianceAfter;
				pts.downstack.x = x;
				pts.downstack.y = y;
				pts.downstack.rt = rt;
			}
			mixPts += varianceAfter;
			mixPts += 800 * (holeBefore - holeAfter);
			mixPts += hchangeAfter - hchangeBefore;

			//Points for line clears
			if(lines > 0){
				pts.downstack.pts += lines;
				pts.downstack.x = x;
				pts.downstack.y = y;
				pts.downstack.rt = rt;
				pts.linesend.x = y;
				pts.linesend.y = y;
				pts.linesend.rt = rt;
				pts.linesend.pts = (lines == 4 ? 4 : lines > 1 ? 2 : 1);
			}

			// All clear
			if(fld.isEmpty()){
				mixPts += 500000;
				pts.linesend.x = x;
				pts.linesend.y = y;
				pts.linesend.rt = rt;
				pts.linesend.pts = 10;
			}
			
			if(lines == 1) mixPts -= 2000;
			if(lines == 2) mixPts -= 1000;
			if(lines == 3) mixPts += 300;
			if(lines >= 4){
				pts.prepareTetris.x = x;
				pts.prepareTetris.y = y;
				pts.prepareTetris.rt = rt;
				pts.prepareTetris.pts = 1;
				mixPts += 3000;
			}
			
			if( (lines < 4) && (!fld.isEmpty()) ) {
				if((tspin) && (lines >= 2)) {
					// T-Spin Bonus - retained from Basic AI, but should never actually trigger
					mixPts += 10000 * lines;
					if(depth >= 0){
						pts.prepareTSD.x = x;
						pts.prepareTSD.y = y;
						pts.prepareTSD.rt = rt;
						pts.prepareTSD.pts = lines;
					}
					pts.linesend.x = x;
					pts.linesend.y = y;
					pts.linesend.rt = rt;
					pts.linesend.pts = lines;
				}
			}
		}
		
		pts.highestPts.x = x;
		pts.highestPts.y = y;
		pts.highestPts.rt = rt;
		pts.highestPts.pts = mixPts;
		return pts;
	}
	//private static final int[][] HI_PENALTY = {{6, 2}, {7, 6}, {6, 2}, {1, 0}};
	public static Piece checkOffset(Piece p, GameEngine engine)
	{
		Piece result = new Piece(p);
		result.big = engine.big;
		if (!p.offsetApplied)
			result.applyOffsetArray(engine.ruleopt.pieceOffsetX[p.id], engine.ruleopt.pieceOffsetY[p.id]);
		return result;
	}
	
	public static int[] calcValleys(int[] depths, int move)
	{
		int[] result = {0, 0, 0};
		if (depths[0] > depths[move])
			result[0] = (depths[0]-depths[move])/3/move;
		if ((move >= 2) && (depths[depths.length-1] > depths[depths.length-move-1]))
			result[0] = (depths[depths.length-1]-depths[depths.length-move-1])/3/move;
		for (int i = move; i < depths.length-move; i+=move)
		{
			int left = depths[i-move], right = depths[i+move];
			int lowerSide = Math.max(left, right);
			int diff = depths[i] - lowerSide;
			if (diff >= 3)
				result[0] += diff/3/move;
			if (left == right)
			{
				if (left == depths[i]+(2*move))
				{
					result[0]++;
					result[1]--;
					result[2]--;
				}
				else if (left == depths[i]+move)
				{
					result[1]++;
					result[2]++;
				}
			}
			if ((diff/move)%4 == 2)
			{
				if (left > right)
					result[1]+=2;
				else if (left < right)
					result[2]+=2;
				else
				{
					result[2]++;
					result[1]++;
				}
			}
		}
		if (((depths[0] - depths[move])/move)%4 == 2)
			result[2] += 2;
		if ((move >= 2) && ((depths[depths.length-1] - depths[depths.length-move-1])/move)%4 == 2)
			result[1] += 2;
		/*
		if ((depthsBefore[width-2] - depthsBefore[width-3])%4 == 2 &&
				(depthsBefore[width-1] - depthsBefore[width-2]) < 2)
			valleysBefore[1]++;
		valleysBefore[2] >>= 1;
		valleysBefore[1] >>= 1;
		*/
		return result;
	}
	
	/**
	 * @deprecated
	 * Workaround for the bug in Field.getHighestBlockY(int).
	 * The bug has since been fixed as of NullpoMino v6.5, so
	 * fld.getHighestBlockY(x) should be equivalent.
	 * @param fld Field
	 * @param x X coord
	 * @return Y coord of highest block
	 */
	@Deprecated
	public static int getColumnDepth (Field fld, int x)
	{
		int maxY = fld.getHeight()-1;
		int result = fld.getHighestBlockY(x);
		if (result == maxY && fld.getBlockEmpty(x, maxY))
			result++;
		return result;
	}

	public static int[] getColumnDepths (Field fld)
	{
		int width = fld.getWidth();
		int[] result = new int[width];
		for (int x = 0; x < width; x++)
			result[x] = fld.getHighestBlockY(x);
		return result;
	}
	/**
	 * Returns the farthest x position the piece can move.
	 * @param x X coord
	 * @param y Y coord
	 * @param dir -1 to move left, 1 to move right.
	 * @param engine GameEngine
	 * @param fld Field
	 * @param piece Piece
	 * @param rt Desired final rotation direction.
	 * @return The farthest x position in the direction that the piece can be moved to.
	 */
	public int mostMovableX (int x, int y, int dir, GameEngine engine, Field fld, Piece piece, int rt)
	{
		if (dir == 0)
			return x;
		int shift = 1;
		if (piece.big)
			shift = 2;
		int testX = x;
		int testY = y;
		int testRt = Piece.DIRECTION_UP;
		SpeedParam speed = engine.speed;
		if (speed.gravity >= 0 && speed.gravity < speed.denominator)
		{
			if (DEBUG_ALL)
				log.debug("mostMovableX not applicable - low gravity (gravity = " +
						speed.gravity + ", denominator = " + speed.denominator + ")");
			if (dir < 0)
				return piece.getMostMovableLeft(testX, testY, rt, fld);
			else if (dir > 0)
				return piece.getMostMovableRight(testX, testY, rt, fld);
		}
		if (piece.id == Piece.PIECE_I && dir > 0)
			return piece.getMostMovableRight(testX, testY, rt, fld);
		boolean floorKickOK = false;
		if ((piece.id == Piece.PIECE_I || piece.id == Piece.PIECE_T) &&
				((engine.nowUpwardWallkickCount < engine.ruleopt.rotateMaxUpwardWallkick
						|| engine.ruleopt.rotateMaxUpwardWallkick < 0) ||
						(engine.stat == GameEngine.Status.ARE)))
			floorKickOK = true;
		testY = piece.getBottom(testX, testY, testRt, fld);
		if (piece.id == Piece.PIECE_T && piece.direction != Piece.DIRECTION_UP)
		{
			int testY2 = piece.getBottom(testX, testY, Piece.DIRECTION_DOWN, fld);
			if (testY2 > testY)
			{
				boolean kickRight = piece.checkCollision(testX+shift, testY2, testRt, fld);
				boolean kickLeft = piece.checkCollision(testX-shift, testY2, testRt, fld);
				if (kickRight)
				{
					testY = testY2;
					if (rt == Piece.DIRECTION_UP)
						testX+=shift;
				}
				else if (kickLeft)
				{
					testY = testY2;
					if (rt == Piece.DIRECTION_UP)
						testX-=shift;
				}
				else if (floorKickOK)
					floorKickOK = false;
				else
					return testX;
			}
		}
		while (true)
		{
			if (!piece.checkCollision(testX+dir, testY, testRt, fld))
				testX += dir;
			else if (testRt != rt)
			{
				testRt = rt;
				if (floorKickOK && piece.checkCollision(testX, testY, testRt, fld))
				{
					if (piece.id == Piece.PIECE_I)
					{
						if (piece.big)
							testY -= 4;
						else
							testY -= 2;
					}
					else
						testY--;
					floorKickOK = false;
				}
			}
			else
			{
				if (DEBUG_ALL)
					log.debug("mostMovableX(" + x + ", " + y + ", " + dir +
							", piece " + Piece.PIECE_NAMES[piece.id] + ", " + rt + ") = " + testX);
				if (piece.id == Piece.PIECE_I && testX < 0 && (rt&1) == 1)
				{
					int height1 = fld.getHighestBlockY(1);
					if (height1 < fld.getHighestBlockY(2) &&
							height1 < fld.getHighestBlockY(3)+2)
						return 0;
					else if (height1 > fld.getHighestBlockY(0))
						return -1;
				}
				return testX;
			}
			testY = piece.getBottom(testX, testY, testRt, fld);
		}
	}

	protected void logBest(int caseNum)
	{
		log.debug("New best position found (Case " + caseNum +
				"): bestHold = " + bestHold +
				", bestX = " + bestX +
				", bestY = " + bestY +
				", bestRt = " + bestRt +
				", bestXSub = " + bestXSub +
				", bestYSub = " + bestYSub +
				", bestRtSub = " + bestRtSub +
				", bestPts = " + bestPts);
	}

	/**
	 * Called to display internal state
	 * @param engine The GameEngine that owns this AI
	 * @param playerID Player ID
	 */
	public void renderState(GameEngine engine, int playerID){
		EventReceiver r = engine.owner.receiver;
		final int offsetx = 50;
		final int offsety = 50;
		r.drawScoreFont(engine, playerID, 24 - offsetx, 40 - offsety, "X", EventReceiver.COLOR_BLUE, 0.5f);
		r.drawScoreFont(engine, playerID, 27 - offsetx, 40 - offsety, "Y", EventReceiver.COLOR_BLUE, 0.5f);
		r.drawScoreFont(engine, playerID, 30 - offsetx, 40 - offsety, "RT", EventReceiver.COLOR_BLUE, 0.5f);
		r.drawScoreFont(engine, playerID, 19 - offsetx, 41 - offsety, "BEST:", EventReceiver.COLOR_BLUE, 0.5f);
		r.drawScoreFont(engine, playerID, 24 - offsetx, 41 - offsety, String.valueOf(bestX), 0.5f);
		r.drawScoreFont(engine, playerID, 27 - offsetx, 41 - offsety, String.valueOf(bestY), 0.5f);
		r.drawScoreFont(engine, playerID, 30 - offsetx, 41 - offsety, String.valueOf(bestRt), 0.5f);
		r.drawScoreFont(engine, playerID, 19 - offsetx, 42 - offsety, "SUB:", EventReceiver.COLOR_BLUE, 0.5f);
		r.drawScoreFont(engine, playerID, 24 - offsetx, 42 - offsety, String.valueOf(bestXSub), 0.5f);
		r.drawScoreFont(engine, playerID, 27 - offsetx, 42 - offsety, String.valueOf(bestYSub), 0.5f);
		r.drawScoreFont(engine, playerID, 30 - offsetx, 42 - offsety, String.valueOf(bestRtSub), 0.5f);
		r.drawScoreFont(engine, playerID, 19 - offsetx, 43 - offsety, "NOW:", EventReceiver.COLOR_BLUE, 0.5f);
		if (engine.nowPieceObject == null)
			r.drawScoreFont(engine, playerID, 24 - offsetx, 43 - offsety, "-- -- --", 0.5f);
		else
		{
			r.drawScoreFont(engine, playerID, 24 - offsetx, 43 - offsety, String.valueOf(engine.nowPieceX), 0.5f);
			r.drawScoreFont(engine, playerID, 27 - offsetx, 43 - offsety, String.valueOf(engine.nowPieceY), 0.5f);
			r.drawScoreFont(engine, playerID, 30 - offsetx, 43 - offsety, String.valueOf(engine.nowPieceObject.direction), 0.5f);
		}
		r.drawScoreFont(engine, playerID, 19 - offsetx, 44 - offsety, "MOVE SCORE:", EventReceiver.COLOR_BLUE, 0.5f);
		r.drawScoreFont(engine, playerID, 31 - offsetx, 44 - offsety, String.valueOf(bestPts), bestPts <= 0, 0.5f);
		r.drawScoreFont(engine, playerID, 19 - offsetx, 45 - offsety, "THINK ACTIVE:", EventReceiver.COLOR_BLUE, 0.5f);
		r.drawScoreFont(engine, playerID, 32 - offsetx, 45 - offsety, GeneralUtil.getOorX(thinking), 0.5f);
		r.drawScoreFont(engine, playerID, 19 - offsetx, 46 - offsety, "IN ARE:", EventReceiver.COLOR_BLUE, 0.5f);
		r.drawScoreFont(engine, playerID, 26 - offsetx, 46 - offsety, GeneralUtil.getOorX(inARE), 0.5f);
		r.drawScoreFont(engine, playerID, 19 - offsetx, 47 - offsety, "BESTHOLD:", EventReceiver.COLOR_BLUE, 0.5f);
		r.drawScoreFont(engine, playerID, 28 - offsetx, 47 - offsety, bestHold ? "YES" : "NO", 0.5f);
	}

	/*
	 * スレッドの処理
	 */
	public void run() {
		log.info("PoochyBot: Thread start");
		threadRunning = true;

		while(threadRunning) {
			try {
				synchronized(thinkRequest)
				{
					if (!thinkRequest.active)
						thinkRequest.wait();
				}
			} catch (InterruptedException e) {
				log.debug("PoochyBot: InterruptedException waiting for thinkRequest signal");
			}
			if(thinkRequest.active) {
				thinkRequest.active = false;
				thinking = true;
				try {
					thinkBestPosition(gEngine, gEngine.playerID);
					thinkComplete = true;
					log.debug("PoochyBot: thinkBestPosition completed successfully");
				} catch (Throwable e) {
					log.debug("PoochyBot: thinkBestPosition Failed", e);
				}
				thinking = false;
			}

			if(thinkDelay > 0) {
				try {
					Thread.sleep(thinkDelay);
				} catch (InterruptedException e) {
					log.debug("PoochyBot: InterruptedException trying to sleep");
				}
			}
		}

		threadRunning = false;
		log.info("PoochyBot: Thread end");
	}
	
	//Wrapper for think requests
	private static class ThinkRequestMutex
	{
		public boolean active;
		public ThinkRequestMutex()
		{
			active = false;
		}
		public synchronized void newRequest()
		{
			active = true;
			notifyAll();
		}
	}
}
